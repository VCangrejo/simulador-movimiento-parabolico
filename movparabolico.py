from math import *
from time import sleep
from graphics import *

Vo=60
t=0
Wo=50*3.14159235/180

y=0
g=9.81

ventana=GraphWin("simulador", 400,400)
ventana.setCoords(0,0,400,400)

while(y>=0):
    x=Vo*cos(Wo)*t
    y=Vo*sin(Wo)*t-(1.0/2)*g*t*t

    micirculo=Circle(Point(x,y),5)
    micirculo.draw(ventana)
    micirculo.setFill('green')
    print(x,y)
    sleep(0.1)
    t=t+0.5

ventana.getMouse()
